/**
 * @param String
*/
function ucfirst(str)
{
    let word = str.trim().toLowerCase();
    let firstLetter = word[0].toUpperCase();
    let restLetter = word.substring(1);

    return (firstLetter+restLetter);
}

function capitalize(str)
{
    let res = "";

    if(!str) {
        return res;
    }

    for(let i = 0; i < str.length; i++) {

        if (i === 0 || str[i - 1] === " ") {
            res += str[i].toUpperCase();
            continue;
        }
        else if (str[i - 1] === undefined) {
            continue;
        }
        else {
            res += str[i];
        }
    }
    return res;
}

function camelCase(str)
{
    let res = capitalize(str);

    res = res.replace(/\s/g, "")
    return res;
}

function snake_case(str)
{
    let res = "";

    if(!str) {
        return res;
    }

    for(let i = 0; i < str.length; i++) {

        if (str[i] === " ") {
            res += "_";
            continue;
        } else {
            res += str[i];
        }
    }
    return res.toLowerCase();
}

function leet(str)
{
    let res = "";

    let cryptage = {
        'a' : 4,
        'e' : 3,
        'i' : 1,
        'o' : 0,
        'u' : '(_)',
        'y' : 7
    }

    for(let letter of str) {
        if(cryptage[letter] !== undefined) {
            res += cryptage[letter]
        } else {
            res += letter;
        }
    }
    return res.toLowerCase();
}